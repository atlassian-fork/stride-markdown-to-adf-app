/**
 * This is a helper class to send messages to a Stride conversation.
 */

const PORT = process.env.PORT;
const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const API_BASE_URL = 'https://api.atlassian.com';

let express = require("express");
let router = express.Router();
const request = require('request');

var exports = module.exports = {};

/**
 * Function to send a JSON message to a Stride conversation.
 */
exports.sendMessage = function sendMessage(cloudId, conversationId, messageTxt, callback) {
    this.getAccessToken(function (err, accessToken) {
      if (err) {
        callback(err);
      } else {
        const uri = API_BASE_URL + '/site/' + cloudId + '/conversation/' + conversationId + '/message';
        const options = {
          uri: uri,
          method: 'POST',
          headers: {
            authorization: "Bearer " + accessToken,
            "cache-control": "no-cache"
          },
          json: {
            body: messageTxt
          }
        }
    
        request(options, function (err, response, body) {
           if(err){
             console.log(err);
           }
         });
       }
     });
   }

/**
 * Boilerplate code to get your Access Token
 */

exports.getAccessToken = function getAccessToken(callback) {
    const options = {
      uri: 'https://auth.atlassian.com/oauth/token',
      method: 'POST',
      json: {
        grant_type: "client_credentials",
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        "audience": "api.atlassian.com"
      }
    };
    request(options, function (err, response, body) {
      if (response.statusCode === 200 && body.access_token) {
        callback(null, body.access_token);
      } else {
        callback("could not generate access token: " + JSON.stringify(response));
      }
    });
  }